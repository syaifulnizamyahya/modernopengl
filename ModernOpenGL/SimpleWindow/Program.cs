﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace SimpleWindow
{
    class Program : GameWindow
    {
        private Gwen.Input.OpenTK _input;
        private Gwen.Renderer.OpenTK _renderer;
        private Gwen.Skin.Base _skin;
        private Gwen.Control.Canvas _canvas;

        /// <summary>
        /// Setup OpenGL and load resources here.
        /// </summary>
        /// <param name="e">Not used.</param>
        protected override void OnLoad(EventArgs e)
        {
            InitializeGwenInput();
            InitializeGwenScene();
        }

        /// <summary>
        /// Respond to resize events here.
        /// </summary>
        /// <param name="e">Contains information on the new GameWindow size.</param>
        /// <remarks>There is no need to call the base implementation.</remarks>
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            _canvas.SetSize(Width, Height);
        }

        /// <summary>
        /// Add your game rendering code here.
        /// </summary>
        /// <param name="e">Contains timing information.</param>
        /// <remarks>There is no need to call the base implementation.</remarks>
        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.DepthBufferBit | ClearBufferMask.ColorBufferBit);
            GL.Viewport(0, 0, Width, Height);

            Render3DScene();

            RenderGwenGui();

            SwapBuffers();
        }

        /// <summary>
        /// Add your game logic here.
        /// </summary>
        /// <param name="e">Contains timing information.</param>
        /// <remarks>There is no need to call the base implementation.</remarks>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            // each cached string is an allocated texture, 
            // flush the cache once in a while in your real project
            if (_renderer.TextCacheSize > 1000)
            {
                _renderer.FlushTextCache();
            }
        }

        public override void Dispose()
        {
            _canvas.Dispose();
            _skin.Dispose();
            _renderer.Dispose();

            base.Dispose();
        }

        #region input events
        /// <summary>
        /// Occurs when a key is pressed.
        /// </summary>
        /// <param name="sender">The KeyboardDevice which generated this event.</param>
        /// <param name="e">The key that was pressed.</param>
        void Keyboard_KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Exit();

            _input.ProcessKeyDown(e);
        }

        void Keyboard_KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            _input.ProcessKeyUp(e);
        }

        void Mouse_ButtonDown(object sender, MouseButtonEventArgs args)
        {
            _input.ProcessMouseMessage(args);
        }

        void Mouse_ButtonUp(object sender, MouseButtonEventArgs args)
        {
            _input.ProcessMouseMessage(args);
        }

        void Mouse_Move(object sender, MouseMoveEventArgs args)
        {
            _input.ProcessMouseMessage(args);
        }

        void Mouse_Wheel(object sender, MouseWheelEventArgs args)
        {
            _input.ProcessMouseMessage(args);
        }
        #endregion

        private void InitializeGwenScene()
        {
            _renderer = new Gwen.Renderer.OpenTK();
            _skin = new Gwen.Skin.TexturedBase(_renderer, "DefaultSkin.png");
            _canvas = new Gwen.Control.Canvas(_skin);
            _canvas.SetSize(Width, Height);

            _input = new Gwen.Input.OpenTK(this);
            _input.Initialize(_canvas);

            ConstructGwenGui();
        }

        private void InitializeGwenInput()
        {
            Keyboard.KeyDown += Keyboard_KeyDown;
            Keyboard.KeyUp += Keyboard_KeyUp;

            Mouse.ButtonDown += Mouse_ButtonDown;
            Mouse.ButtonUp += Mouse_ButtonUp;
            Mouse.Move += Mouse_Move;
            Mouse.WheelChanged += Mouse_Wheel;
        }

        private void ConstructGwenGui()
        {
            var button = new Gwen.Control.Button(_canvas);
            button.SetText("Hello world");
        }

        private void Render3DScene()
        {
        }

        private void RenderGwenGui()
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, Width, Height, 0, -1, 1);

            _canvas.RenderCanvas();
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var program = new Program())
            {
                program.Width = 640;
                program.Height = 480;
                program.Title = "Gwen-DotNet OpenTK test";
                program.Run();
            }
        }
    }
}
