﻿using System;
using System.Diagnostics;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.IO;
using OpenTK.Input;

namespace HelloGL3
{
    public class Program : GameWindow
    {

#region 3D scene variables
        int _vertexShaderHandle,
        _fragmentShaderHandle,
        _shaderProgramHandle,
        _modelviewMatrixLocation,
        _projectionMatrixLocation,
        _vaoHandle,
        _positionVboHandle,
        _normalVboHandle,
        _eboHandle;

        readonly Vector3[] _positionVboData =
        {
            new Vector3(-1.0f, -1.0f,  1.0f),
            new Vector3( 1.0f, -1.0f,  1.0f),
            new Vector3( 1.0f,  1.0f,  1.0f),
            new Vector3(-1.0f,  1.0f,  1.0f),
            new Vector3(-1.0f, -1.0f, -1.0f),
            new Vector3( 1.0f, -1.0f, -1.0f), 
            new Vector3( 1.0f,  1.0f, -1.0f),
            new Vector3(-1.0f,  1.0f, -1.0f) };

        readonly int[] _indicesVboData =
        {
            // front face
            0, 1, 2, 2, 3, 0,
            // top face
            3, 2, 6, 6, 7, 3,
            // back face
            7, 6, 5, 5, 4, 7,
            // left face
            4, 0, 3, 3, 7, 4,
            // bottom face
            0, 1, 5, 5, 4, 0,
            // right face
            1, 5, 6, 6, 2, 1, };

        Matrix4 _projectionMatrix, _modelviewMatrix;
#endregion

        protected override void OnLoad(EventArgs e)
        {
            Initialize3DScene();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            Update3DSceneMatrix();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {

            Draw3DScene();

            SwapBuffers();
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            Update3DSceneInput();
            Update3DSceneAnimation(e.Time);
        }

        private void Initialize3DScene()
        {
            CreateShaders();
            CreateVBOs();
            CreateVAOs();

            // Other state
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.CullFace);

        }

        void CreateShaders()
        {
            _vertexShaderHandle = GL.CreateShader(ShaderType.VertexShader);
            _fragmentShaderHandle = GL.CreateShader(ShaderType.FragmentShader);

            using (var vertexShaderSource = new StreamReader("shader.vert"))
            {
                GL.ShaderSource(_vertexShaderHandle, vertexShaderSource.ReadToEnd());
            }

            using (var fragmentShaderSource = new StreamReader("shader.frag"))
            {
                GL.ShaderSource(_fragmentShaderHandle, fragmentShaderSource.ReadToEnd());
            }

            GL.CompileShader(_vertexShaderHandle);
            GL.CompileShader(_fragmentShaderHandle);

            Debug.WriteLine(GL.GetShaderInfoLog(_vertexShaderHandle));
            Debug.WriteLine(GL.GetShaderInfoLog(_fragmentShaderHandle));

            // Create program
            _shaderProgramHandle = GL.CreateProgram();

            GL.AttachShader(_shaderProgramHandle, _vertexShaderHandle);
            GL.AttachShader(_shaderProgramHandle, _fragmentShaderHandle);

            GL.BindAttribLocation(_shaderProgramHandle, 0, "in_position");
            GL.BindAttribLocation(_shaderProgramHandle, 1, "in_normal");

            GL.LinkProgram(_shaderProgramHandle);
            Debug.WriteLine(GL.GetProgramInfoLog(_shaderProgramHandle));
            GL.UseProgram(_shaderProgramHandle);

            // Set uniforms
            _projectionMatrixLocation = GL.GetUniformLocation(_shaderProgramHandle, "projection_matrix");
            _modelviewMatrixLocation = GL.GetUniformLocation(_shaderProgramHandle, "modelview_matrix");

            float aspectRatio;
            // Prevent divide by zero
            if (ClientSize.Height != 0)
            {
                aspectRatio = ClientSize.Width/(float) (ClientSize.Height);
            }
            else
                aspectRatio = 1.3f;

            Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, aspectRatio, 1, 100, out _projectionMatrix);
            _modelviewMatrix = Matrix4.LookAt(new Vector3(0, 3, 5), new Vector3(0, 0, 0), new Vector3(0, 1, 0));

            GL.UniformMatrix4(_projectionMatrixLocation, false, ref _projectionMatrix);
            GL.UniformMatrix4(_modelviewMatrixLocation, false, ref _modelviewMatrix);
        }

        void CreateVBOs()
        {
            _positionVboHandle = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, _positionVboHandle);
            GL.BufferData<Vector3>(BufferTarget.ArrayBuffer,
                new IntPtr(_positionVboData.Length * Vector3.SizeInBytes),
                _positionVboData, BufferUsageHint.StaticDraw);

            _normalVboHandle = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, _normalVboHandle);
            GL.BufferData<Vector3>(BufferTarget.ArrayBuffer,
                new IntPtr(_positionVboData.Length * Vector3.SizeInBytes),
                _positionVboData, BufferUsageHint.StaticDraw);

            _eboHandle = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _eboHandle);
            GL.BufferData(BufferTarget.ElementArrayBuffer,
                new IntPtr(sizeof(uint) * _indicesVboData.Length),
                _indicesVboData, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        void CreateVAOs()
        {
            // GL3 allows us to store the vertex layout in a "vertex array object" (VAO).
            // This means we do not have to re-issue VertexAttribPointer calls
            // every time we try to use a different vertex layout - these calls are
            // stored in the VAO so we simply need to bind the correct VAO.
            _vaoHandle = GL.GenVertexArray();
            GL.BindVertexArray(_vaoHandle);

            GL.EnableVertexAttribArray(0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _positionVboHandle);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes, 0);

            GL.EnableVertexAttribArray(1);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _normalVboHandle);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, true, Vector3.SizeInBytes, 0);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _eboHandle);

            GL.BindVertexArray(0);
        }


        private void Update3DSceneMatrix()
        {
            float aspectRatio;
            // Prevent divide by zero
            if (ClientSize.Height != 0)
            {
                aspectRatio = ClientSize.Width / (float)(ClientSize.Height);
            }
            else
                aspectRatio = 1.3f;

            Matrix4.CreatePerspectiveFieldOfView((float)Math.PI / 4, aspectRatio, 1, 100, out _projectionMatrix);

            GL.UniformMatrix4(_projectionMatrixLocation, false, ref _projectionMatrix);
        }
        
        private void Update3DSceneInput()
        {
            var keyboard = OpenTK.Input.Keyboard.GetState();
            if (keyboard[Key.Escape])
                Exit();
        }

        private void Update3DSceneAnimation(double time)
        {
            Matrix4 rotation = Matrix4.CreateRotationY((float)time);
            Matrix4.Mult(ref rotation, ref _modelviewMatrix, out _modelviewMatrix);
            GL.UniformMatrix4(_modelviewMatrixLocation, false, ref _modelviewMatrix);
        }


        private void Draw3DScene()
        {
            GL.Viewport(0, 0, ClientSize.Width, ClientSize.Height);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.BindVertexArray(_vaoHandle);
            GL.DrawElements(PrimitiveType.Triangles, _indicesVboData.Length,
                DrawElementsType.UnsignedInt, IntPtr.Zero);
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var program = new Program())
            {
                program.Width = 640;
                program.Height = 480;
                program.Title = "OpenTK 3 Example";
                program.Run();
            }
        }
    }
}